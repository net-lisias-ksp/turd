A word from our sponsor...

To install, simply place these files so the TURD folder resides within the GameData directory of your KSP install. Textures Unlimited and Module Manager are not included but required for this pack to function, as is the B9 Procedural Wings mod.  You will also need the stock recolour pack.

Textures Unlimited:
https://forum.kerbalspaceprogram.com/index.php?/topic/167450-19x-textures-unlimited-pbr-shader-texture-set-and-model-loading-api/

Module Manager:
https://forum.kerbalspaceprogram.com/index.php?/topic/50533-18x-110x-module-manager-414-july-7th-2020-locked-inside-edition/

B9 Procedural Wings:
https://forum.kerbalspaceprogram.com/index.php?/topic/175197-13x14x15x16x17x18x19x-b9-procedural-wings-fork-go-big-or-go-home-update-40-larger-wings/

This pack contains scratch made textures, config files and modified textures. Any modified textures were originally created by Bac9 (to the best of my knowledge). The textures contained within this pack were created and modified for the purposes of adding PBR recolour shaders made available by Textures Unlimited (by Shadowmage) to the parts specified in the configs, for use in Kerbal Space Program by Squad (which should be obvious, right?)

I claim no rights to the original works by others and distribute modified files with permission, either granted personally via messaging or implied by licensing and / or general addon posting rules laid out by Squad.


Message ends...

Manwith Noname.

If you modify this pack under the issued license, please distribute this message intact and note your own modifications above it.