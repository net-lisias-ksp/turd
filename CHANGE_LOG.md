# TURD - Textures Unlimited Recolour Depot :: Change Log

* 2022:0324: Stock Recolour 0.3.10 (Manwith Noname) for KSP 1.12.x
	+ No changelog provided
* 2021:1006: Stock Recolour 0.3.8 (Manwith Noname) for KSP 1.12.2
	+ No changelog provided
* 2021:0130: Stock Recolour 0.3.7 (Manwith Noname) for KSP 1.11
	+ No changelog provided
* 2021:0108: Stock Recolour 0.3.5 (Manwith Noname) for KSP 1.10
	+ No changelog provided
* 2020:0701: Stock Recolour 0.3.3 (Manwith Noname) for KSP 1.9
	+ No changelog provided
* 2020:0701: AirplanePlus 0.1.0  (Manwith Noname) for KSP 1.9
	+ No changelog provided
* 2020:0214: Mk3 Expansion 0.2.1 (Manwith Noname) for KSP 1.8
	+ No changelog provided
* 2020:0214: Mk2 Expansion 0.2.1 (Manwith Noname) for KSP 1.8
	+ No changelog provided
* 2019:1120: Stock Recolour 0.3.3 (Manwith Noname) for KSP 1.8
	+ No changelog provided
* 2019:1119: MKIV Expansion 0.1.0 (Manwith Noname) for KSP 1.8
	+ No changelog provided
* 2019:1119: Procedural Wings 0.1.0 (Manwith Noname) for KSP 1.8
	+ No changelog provided
* 2019:1020: OPT Recolour 0.2.0 (Manwith Noname) for KSP 1.7
	+ No changelog provided
* 2019:1020: Mk3 Expansion 0.2.1 (Manwith Noname) for KSP 1.7
	+ No changelog provided
* 2019:1020: Mk2 Expansion 0.2.0 (Manwith Noname) for KSP 1.7
	+ No changelog provided
* 2019:1020: Stock Recolour 0.3.1 (Manwith Noname) for KSP 1.7
	+ No changelog provided
* 2018:0924: Stock Extras 0.1.0 (Manwith Noname) for KSP 1.4
	+ No changelog provided
* 2018:0923: OPT Recolour 0.1.0 (Manwith Noname) for KSP 1.4
	+ No changelog provided
* 2018:0923: Mk3 Expansion 0.1.1 (Manwith Noname) for KSP 1.4
	+ No changelog provided
* 2018:0923: Mk2 Expansion 0.1.0 (Manwith Noname) for KSP 1.4
	+ No changelog provided
* 2018:0923: Stock Recolour 0.2.8 (Manwith Noname) for KSP 1.4
	+ No changelog provided
